﻿using LoanCalculatorRepository.Context;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public abstract class BaseRepository
    {
        private readonly IContext _context;

        public BaseRepository(IContext context)
        {
            _context = context;
        }

        protected async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
