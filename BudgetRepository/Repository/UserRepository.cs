﻿using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Context;
using Microsoft.EntityFrameworkCore;
using System;
using BC = BCrypt.Net.BCrypt;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace LoanCalculatorRepository.Repository
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        private readonly DbSet<UserIdentityModel> _users;

        public UserRepository(IContext context) : base(context)
        {
            _users = context.Users;
        }
        public IQueryable<UserIdentityModel> GetAll()
        {
            return _users.Where(x => !x.IsDeleted);
        }

        public Task<UserIdentityModel> GetById(Guid Id)
        {
            return _users.FirstOrDefaultAsync(x => x.UniqueId == Id);
        }

        public async Task<(UserIdentityModel, List<string>)> GetByEmailAndPasswordAsync(string email, string password)
        {
            var user = await _users.FirstOrDefaultAsync(x => x.EmailAddress == email);

            if (user is null) return (null, new List<string> { "User Not Found" });

            var match = BC.Verify(password, user.PasswordHash);

            if (!match) return (null, new List<string> { "User Credentials did not match" });

            return (user, null);
        }

        public async Task<(UserIdentityModel, List<string>)> AddAsync(string email, string password)
        {
            try
            {
                var newUserId = Guid.NewGuid();
                string passwordHash = BC.HashPassword(password);

                var user = await _users.AddAsync(new UserIdentityModel
                {
                    PasswordHash = passwordHash,
                    DateCreated = DateTime.Now,
                    CreatedBy = Guid.NewGuid(),
                    UniqueId = newUserId,
                    EmailAddress = email,
                    IsDeleted = false
                });

                await SaveChangesAsync();

                return (user.Entity, null);
            }
            catch (Exception ex)
            {
                return (null, new List<string> { ex.Message });
            }
        }

        public async Task<UserIdentityModel> DeleteAsync(Guid uniqueId, string reason, Guid actor)
        {
            var entity = await GetById(uniqueId);
            entity.IsDeleted = true;

            return await Update(entity);
        }

        private async Task<UserIdentityModel> Update(UserIdentityModel entity)
        {
            var user = _users.Update(entity);

            await SaveChangesAsync();

            return user.Entity;
        }


    }
}
