﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        Task<T> GetById(Guid Id);

        Task<T> DeleteAsync(Guid uniqueId, string reason, Guid actor);
    }
}
