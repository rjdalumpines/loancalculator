﻿using LoanCalculatorDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoanCalculatorRepository.Repository.Interfaces
{
    interface ITitleRepository: IRepository<Title>
    {
    }
}
