﻿using LoanCalculatorDomain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public interface ILoanRepository : IRepository<Loan>
    {
        Task<Loan> CreateAsync(decimal interestRate, decimal monthlyAmortization, decimal principal, Guid requesterId, int terms);

        IEnumerable<Loan> GetByRequesterId(Guid id);
    }
}
