﻿using LoanCalculatorDomain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public interface IUserRepository : IRepository<UserIdentityModel>
    {
        Task<(UserIdentityModel, List<string>)> GetByEmailAndPasswordAsync(string email, string password);
        Task<(UserIdentityModel, List<string>)> AddAsync(string email, string password);
    }
}