﻿using LoanCalculatorDomain.Model;
using System;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public interface IRequesterRepository : IRepository<Requester>
    {
        Task<Requester> CreateAsync(string emailAddress, string middleName, string firstName, string lastName, string mobileNumber, Guid? titleUniqueId, string extensionName);

        Task<Requester> UpdateAsync(string emailAddress, string middleName, string firstName, string lastName, string mobileNumber, Guid? titleUniqueId, string extensionName, Guid uniqueId);
    }
}