﻿using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public class RequesterRepository : BaseRepository, IRequesterRepository
    {
        private readonly DbSet<Requester> _requester;

        public RequesterRepository(IContext context) : base(context)
        {
            _requester = context.Requesters;
        }

        public IQueryable<Requester> GetAll()
        {
            return _requester.Where(x => !x.IsDeleted);
        }

        public async Task<Requester> GetById(Guid Id)
        {
            return await _requester.FirstOrDefaultAsync(x => x.UniqueId == Id);
        }

        public async Task<Requester> CreateAsync(string emailAddress, string middleName, string firstName, string lastName, string mobileNumber, Guid? titleUniqueId, string extensionName)
        {
            var uniqueId = Guid.NewGuid();
            var requestor = new Requester
            {
                IsDeleted = false,
                DateCreated = DateTime.Now,
                CreatedBy = uniqueId,
                UniqueId = uniqueId,
                EmailAddress = emailAddress,
                MiddleName = middleName,
                FirstName = firstName,
                LastName = lastName,
                MobileNumber = mobileNumber,
                TitleUniqueId = titleUniqueId,
                ExtensionName = extensionName
            };

            return await AddAsync(requestor);
        }

        public async Task<Requester> AddAsync(Requester entity)
        {
            var loan = await _requester.AddAsync(entity);

            await SaveChangesAsync();

            return loan.Entity;
        }

        public async Task<Requester> DeleteAsync(Guid uniqueId, string reason, Guid actor)
        {
            var entity = await GetById(uniqueId);
            entity.IsDeleted = true;

            return await Update(entity);
        }

        public async Task<Requester> UpdateAsync(string emailAddress, string middleName, string firstName, string lastName, string mobileNumber, Guid? titleUniqueId, string extensionName, Guid uniqueId)
        {
            var requester = await GetById(uniqueId);

            if (requester is null) return null;

            requester.EmailAddress = emailAddress;
            requester.MiddleName = middleName;
            requester.FirstName = firstName;
            requester.LastName = lastName;
            requester.MobileNumber = mobileNumber;
            requester.TitleUniqueId = titleUniqueId;
            requester.ExtensionName = extensionName;

            return await Update(requester);

        }

        private async Task<Requester> Update(Requester entity)
        {
            try
            {
                var requester = _requester.Update(entity);

                await SaveChangesAsync();

                return requester.Entity;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
