﻿
using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Repository
{
    public class LoanRepository : BaseRepository, ILoanRepository
    {
        private DbSet<Loan> _loans;
        public LoanRepository(IContext context) : base(context)
        {
            _loans = context.Loans;
        }

        public IQueryable<Loan> GetAll()
        {
            return _loans.Where(x => !x.IsDeleted);
        }

        public async Task<Loan> GetById(Guid Id)
        {
            return await _loans.FirstOrDefaultAsync(x => x.UniqueId == Id);
        }

        public async Task<Loan> CreateAsync(decimal interestRate, decimal monthlyAmortization, decimal principal, Guid requesterId, int terms)
        {
            var loan = new Loan
            {
                InterestRate = interestRate,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                CreatedBy = requesterId,
                UniqueId = Guid.NewGuid(),
                MonthlyAmortization = monthlyAmortization,
                Principal = principal,
                RequesterUniqueId = requesterId,
                Terms = terms
            };

            return await AddAsync(loan);
        }

        public async Task<Loan> AddAsync(Loan entity)
        {
            var loan = await _loans.AddAsync(entity);
            await SaveChangesAsync();

            return loan.Entity;
        }



        public async Task<Loan> UpdateAsync(Loan entity)
        {
            var loan = _loans.Update(entity);
            await SaveChangesAsync();

            return loan.Entity;
        }

        public IEnumerable<Loan> GetByRequesterId(Guid id)
        {
            return _loans.Where(x => x.RequesterUniqueId == id && !x.IsDeleted);
        }

        public async Task<Loan> DeleteAsync(Guid uniqueId, string reason, Guid actor)
        {
            var loan = await GetById(uniqueId);
            loan.IsDeleted = true;

           return await Update(loan);
        }

        private async Task<Loan> Update(Loan entity)
        {
            var updatedLoan = _loans.Update(entity);

            await SaveChangesAsync();

            return updatedLoan.Entity;
        }
    }
}
