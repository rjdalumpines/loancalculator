﻿using Microsoft.EntityFrameworkCore;

namespace LoanCalculatorRepository.Context
{
    public class LoanCalculatorSQLServerContext : BaseContext, IContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PCM-9CD48D3;Initial Catalog=LoanCalculator;Integrated Security=True");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
