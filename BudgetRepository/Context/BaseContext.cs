﻿using LoanCalculatorDomain.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace LoanCalculatorRepository.Context
{
    public class BaseContext : DbContext
    {
        public DbSet<Loan> Loans { get; set; }
        public DbSet<Requester> Requesters { get; set; }
        public DbSet<UserIdentityModel> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Loan>().ToTable("Loan");
            modelBuilder.Entity<UserIdentityModel>().ToTable("User");


            modelBuilder.Entity<Loan>().HasKey(c => c.Id);
            modelBuilder.Entity<Requester>().HasKey(c => c.Id);
            modelBuilder.Entity<Title>().HasKey(c => c.Id);


            base.OnModelCreating(modelBuilder);
        }
    }
}
