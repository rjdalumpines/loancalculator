﻿using LoanCalculatorDomain.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorRepository.Context
{
    public interface IContext
    {
        DbSet<Loan> Loans { get; set; }
        DbSet<UserIdentityModel> Users { get; set; }
        DbSet<Requester> Requesters { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
