﻿using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace LoanCalculatorRepository.Context
{
    public class LoanCalculatorSQLiteContext : BaseContext, IContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PCM-9CD48D3;Initial Catalog=LoanCalculator;Integrated Security=True");
            base.OnConfiguring(optionsBuilder);
        }

    }
}
