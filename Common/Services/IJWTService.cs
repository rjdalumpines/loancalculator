﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Services
{
    public interface IJWTService
    {
        string GenerateJSONWebToken(string key, string issuer);
    }
}
