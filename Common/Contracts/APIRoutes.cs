﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Contracts
{
    public static class APIRoutes
    {
        public const string Root = "api";

        public const string Version = "v1";
        public static string BaseURL => $"{Root}/{Version}";

        public static class LoanRoute
        {
            public static string LoanBaseURL => $"{BaseURL}/Loan";

        }

        public static class RequesterRoute
        {
            public static string LoanBaseURL => $"{BaseURL}/Requester";

        }
    }
}
