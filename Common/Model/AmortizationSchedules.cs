﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    public class AmortizationSchedules
    {
        public decimal InterestPaid { get; set; }

        public decimal Principal { get; set; }

        public decimal MonthlyAmortization { get; set; }

        public decimal RunningInterest { get; set; }

        public decimal RunningBalance { get; set; }
    }
}
