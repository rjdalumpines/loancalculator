﻿using Common.Formula.Interfaces;
using System;

namespace Common.Formula
{
    public class MortgagePayments : IMonthlyMortgagePayments
    {
        public decimal GetMonthlyMortgage(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var totalNumberOfPeriods = CalculateTotalNumberOfPeriods(totalNumberOfPayments);

            return CalculateMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPeriods);
        }

        public decimal GetTotalInterest(int totalNumberOfPayments, decimal monthlyMortgage, decimal principalLoanAmount)
        {
            var totalNumberOfPeriods = CalculateTotalNumberOfPeriods(totalNumberOfPayments);

            return (monthlyMortgage * totalNumberOfPeriods) - principalLoanAmount;
        }

        public (decimal MonthlyMortgage, decimal TotalInterest) GetMonthlyMortgageAndTotalInterest(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var totalNumberOfPeriods = CalculateTotalNumberOfPeriods(totalNumberOfPayments);
            var mortgage = CalculateMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPeriods);
            var interest = GetTotalInterest(totalNumberOfPayments, mortgage, principalLoanAmount);

            return (mortgage, interest);
        }

        private decimal CalculateMonthlyMortgage(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPeriods)
        {
            var interestToDecimal = interestRate / 100;
            var monthlyInterestRate = interestToDecimal / Constants.Year.TotalMonths;
            var monthlyInterestOverPeriod = Math.Pow(Convert.ToDouble(1 + monthlyInterestRate), totalNumberOfPeriods);
            var numerator = monthlyInterestRate * Convert.ToDecimal(monthlyInterestOverPeriod);
            var denominator = Convert.ToDecimal(monthlyInterestOverPeriod - 1);

            return Convert.ToDecimal(principalLoanAmount * (numerator / denominator));
        }

        private int CalculateTotalNumberOfPeriods(int totalNumberOfPayments)
        {
            return totalNumberOfPayments * Constants.Year.TotalMonths;
        }
    }
}
