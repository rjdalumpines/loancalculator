﻿namespace Common.Formula.Interfaces
{
    public interface IMonthlyMortgagePayments
    {
        decimal GetMonthlyMortgage(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments);

        decimal GetTotalInterest(int totalNumberOfPayments, decimal monthlyMortgage, decimal principalLoanAmount);

        (decimal MonthlyMortgage, decimal TotalInterest) GetMonthlyMortgageAndTotalInterest(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments);
    }
}