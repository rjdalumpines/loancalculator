﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Formula.Interfaces
{
    public interface IMonthlyAmortizationSchedule
    {
        List<AmortizationSchedules> GetMonthlyAmortizationSchedule(decimal interest, decimal loanedPrincipal, decimal monthlyAmount, int terms);
    }

}
