﻿using Common.Formula.Interfaces;
using Common.Model;
using System.Collections.Generic;

namespace Common.Formula
{
    public class MonthlyAmortizationSchedule : IMonthlyAmortizationSchedule
    {
        public List<AmortizationSchedules> GetMonthlyAmortizationSchedule(decimal interest, decimal loanedPrincipal, decimal monthlyAmount, int terms)
        {
            var amortizationSchedule = new List<AmortizationSchedules>();
            var interestToDecimal = interest / 100;
            var monthlyInterest = interestToDecimal / Constants.Year.TotalMonths;
            var currentBalance = loanedPrincipal;
            var totalInterestPaid = 0M;

            while (currentBalance > 0)
            {
                var (InterestPaid, Principal, Balance) = CalculatCurrentSchedule(currentBalance, monthlyInterest, monthlyAmount);

                currentBalance = Balance;
                totalInterestPaid += InterestPaid;
                amortizationSchedule.Add(new AmortizationSchedules
                {
                    InterestPaid = InterestPaid,
                    Principal = Principal,
                    RunningBalance = Balance,
                    RunningInterest = totalInterestPaid
                });
            }

            return amortizationSchedule;
        }

        private (decimal InterestPaid, decimal Principal, decimal Balance) CalculatCurrentSchedule(decimal currentBalance, decimal monthlyInterest, decimal monthlyAmount)
        {
            var currentInterest = currentBalance * monthlyInterest;
            var currentPrincipal = monthlyAmount - currentInterest;
            var newBalance = currentBalance - currentPrincipal;
            var runningBalance = newBalance < 0 ? 0 : newBalance;

            return (currentInterest, currentPrincipal, runningBalance);
        }
    }
}
