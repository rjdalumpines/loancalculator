﻿
using LoanCalculatorDomain.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Queries
{
    public class GetLoanQuery : IRequest<Loan>
    {
        public Guid Id { get; }

        public GetLoanQuery(Guid id)
        {
            Id = id;
        }
    }
}
