﻿using LoanCalculatorDomain.Model;
using MediatR;
using System;
using System.Collections.Generic;

namespace LoanCalculatorAPI.Queries
{
    public class GetRequesterQuery : IRequest<Requester>
    {
        public Guid Id { get; }

        public GetRequesterQuery(Guid id)
        {
            Id = id;
        }
    }
}
