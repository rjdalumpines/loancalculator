﻿using LoanCalculatorDomain.Model;
using MediatR;
using System;
using System.Collections.Generic;

namespace LoanCalculatorAPI.Queries
{
    public class GetLoansByRequesterQuery : IRequest<List<Loan>>
    {
        public Guid Id { get; }

        public GetLoansByRequesterQuery(Guid id)
        {
            Id = id;
        }
    }
}
