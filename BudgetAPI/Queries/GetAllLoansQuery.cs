﻿using LoanCalculatorDomain.Model;
using MediatR;
using System;
using System.Collections.Generic;

namespace LoanCalculatorAPI.Queries
{
    public class GetAllLoansQuery : IRequest<List<Loan>>
    {

    }
}
