﻿using LoanCalculatorAPI.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Queries
{
    public class GetUserQuery : IRequest<Result<RequesterDTO>>
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}
