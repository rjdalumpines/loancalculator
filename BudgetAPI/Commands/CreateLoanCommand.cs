﻿using LoanCalculatorAPI.Model;
using MediatR;
using System;

namespace LoanCalculatorAPI.Commands
{
    public class CreateLoanCommand : IRequest<LoanRequesterDTO>
    {
        public decimal Principal { get; set; }
        public decimal InterestRate { get; set; }
        public decimal MonthlyAmortization { get; set; }
        public int Terms { get; set; }
        public Guid RequesterUniqueId { get; set; }
    }
}
