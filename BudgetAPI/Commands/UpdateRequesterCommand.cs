﻿using LoanCalculatorAPI.Model;
using LoanCalculatorDomain.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Commands
{
    public class UpdateRequesterCommand : IRequest<Result<RequesterDTO>>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string ExtensionName { get; set; }

        public string MobileNumber { get; set; }

        public string EmailAddress { get; set; }

        public Guid? TitleUniqueId { get; set; }

        public Guid RequesterUniqueId { get; set; }
    }
}
