﻿using LoanCalculatorAPI.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Commands
{
    public class CreateAmortizationScheduleCommand : IRequest<List<AmortizationScheduleDTO>>
    {
        public decimal Interest { get; set; }
        public decimal Principal { get; set; }
        public decimal MonthlyAmortization { get; set; }
        public int Terms { get; set; }

    }
}
