﻿using LoanCalculatorAPI.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Commands
{
    public class ComputeTotalInterestCommand : IRequest<TotalInterestDTO>
    {
        public int Terms { get; set; }
        public decimal Principal { get; set; }
        public decimal MonthlyAmortization { get; set; }
    }
}
