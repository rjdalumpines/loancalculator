﻿using LoanCalculatorAPI.Model;
using MediatR;

namespace LoanCalculatorAPI.Commands
{
    public class RegisterUserCommand : IRequest<Result<RegisterDTO>>
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}