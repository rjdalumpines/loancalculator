﻿using LoanCalculatorAPI.Model;
using MediatR;

namespace LoanCalculatorAPI.Commands
{
    public class ComputeMonthlyAmortizationCommand : IRequest<AmortizationDTO>
    {
        public decimal Principal { get; set; }
        public decimal InterestRate { get; set; }
        public int Terms { get; set; }
    }
}
