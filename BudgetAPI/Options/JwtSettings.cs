﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Options
{
    public class JwtSettings
    {
        public string Secret { get; set; }
    }
}
