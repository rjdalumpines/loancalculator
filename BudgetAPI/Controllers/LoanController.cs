﻿using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace BudgetAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly IMediator _mediator;
        private IConfiguration _config;

        public LoanController(IMediator mediator, IConfiguration config)
        {
            _mediator = mediator;
            _config = config;
        }

        [HttpGet("requester/{id}")]
        public async Task<IActionResult> GetLoansByRequester(Guid id)
        {
            var query = new GetLoansByRequesterQuery(id);
            var result = await _mediator.Send(query);

            if (result is null) return NotFound();

            return Ok(result);
        }

        [HttpGet("loan/{id}")]
        public async Task<IActionResult> GetLoan(Guid id)
        {
            var query = new GetLoanQuery(id);
            var result = await _mediator.Send(query);

            if (result is null) return NotFound();

            return Ok(result);
        }

        [HttpGet("loans")]
        public async Task<IActionResult> GetLoans()
        {
            var query = new GetAllLoansQuery();
            var result = await _mediator.Send(query);

            if (result is null) return NotFound();

            return Ok(result);
        }


        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateLoanAsync([FromBody] CreateLoanCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            //return CreatedAtRoute(nameof(GetLoan), new { UniqueId = result.LoanUniqueid });
            return Ok(result);
        }

        [HttpPost]
        [Route("amortization")]
        public async Task<IActionResult> ComputeMonthlyAmortization([FromBody] ComputeMonthlyAmortizationCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return Ok(result);
        }

        [HttpPost]
        [Route("interest")]
        public async Task<IActionResult> ComputeTotalInterest([FromBody] ComputeTotalInterestCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return Ok(result);
        }

        /// <summary>
        /// Computes the monthly amortization payment and the total interest
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("rates")]
        public async Task<IActionResult> ComputeMonthlyAmortizationAndTotalInterest([FromBody] ComputeMonthlyAmortizationAndTotalInterestCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return Ok(result);
        }

        [HttpPost]
        [Route("schedule")]
        public async Task<IActionResult> GetAmortizationSchedule([FromBody] CreateAmortizationScheduleCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return Ok(result);
        }
    }
}
