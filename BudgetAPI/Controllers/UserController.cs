﻿using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Controllers
{
    [EnableCors()]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] GetUserQuery getUserCommand)
        {
            var result = await _mediator.Send(getUserCommand);

            if (!result.Success) return BadRequest(result);

            return Ok(result);

        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand createUserCommand)
        {
            var result = await _mediator.Send(createUserCommand);

            if (result is null) return BadRequest();

            return Ok(result);

        }
    }
}
