﻿using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Controllers
{
    [Route("api/[controller]")]
    public class RequesterController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RequesterController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("requesters")]
        public async Task<IActionResult> GetRequesters()
        {
            var query = new GetAllRequestersQuery();
            var result = await _mediator.Send(query);

            return Ok(result);
        }

        [HttpGet("getby/{id}")]
        public async Task<IActionResult> GetRequester(Guid id)
        {
            var query = new GetRequesterQuery(id);
            var result = await _mediator.Send(query);

            if (result is null) return NotFound();

            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateRequesterAsync([FromBody] CreateRequesterCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return CreatedAtRoute(nameof(GetRequester), new { UniqueId = result.RequesterUniqueId });
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateRequester([FromBody] UpdateRequesterCommand parameters)
        {
            var result = await _mediator.Send(parameters);

            if (result is null) return BadRequest();

            return Ok(result);
        }
    }
}
