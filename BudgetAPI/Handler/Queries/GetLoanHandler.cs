﻿using LoanCalculatorAPI.Queries;
using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Queries
{
    public class GetLoanHandler : IRequestHandler<GetLoanQuery, Loan>
    {
        private readonly ILoanRepository _loanRepository;

        public GetLoanHandler(ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
        }

        public async Task<Loan> Handle(GetLoanQuery request, CancellationToken cancellationToken)
        {
            var loans = await _loanRepository.GetById(request.Id);

            return loans;
        }
    }
}
