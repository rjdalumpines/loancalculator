﻿using LoanCalculatorDomain.Model;
using LoanCalculatorAPI.Queries;
using LoanCalculatorRepository.Repository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace LoanCalculatorAPI.Handler
{
    public class GetRequesterHandler : IRequestHandler<GetRequesterQuery, Requester>
    {
        private readonly IRequesterRepository _requestRepository;

        public GetRequesterHandler(IRequesterRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        public async Task<Requester> Handle(GetRequesterQuery request, CancellationToken cancellationToken)
        {
            var requester = await _requestRepository.GetById(request.Id);

            return requester;
        }
    }
}
