﻿
using LoanCalculatorAPI.Queries;
using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Queries
{
    public class GetAllLoansHandler : IRequestHandler<GetAllLoansQuery, List<Loan>>
    {
        private readonly ILoanRepository _loanRepository;

        public GetAllLoansHandler(ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
        }

        public async Task<List<Loan>> Handle(GetAllLoansQuery request, CancellationToken cancellationToken)
        {
            var loans = await Task.FromResult(_loanRepository.GetAll());

            return loans.ToList();
        }
    }
}
