﻿using LoanCalculatorDomain.Model;
using LoanCalculatorAPI.Queries;
using LoanCalculatorRepository.Repository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace LoanCalculatorAPI.Handler
{
    public class GetLoansByRequesterHandler : IRequestHandler<GetLoansByRequesterQuery, List<Loan>>
    {
        private readonly ILoanRepository _loanRepository;

        public GetLoansByRequesterHandler(ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
        }

        public async Task<List<Loan>> Handle(GetLoansByRequesterQuery request, CancellationToken cancellationToken)
        {
            var requester = await Task.FromResult(_loanRepository.GetByRequesterId(request.Id));

            return requester.ToList();
        }
    }
}
