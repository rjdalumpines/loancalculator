﻿using LoanCalculatorDomain.Model;
using LoanCalculatorAPI.Queries;
using LoanCalculatorRepository.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler
{
    public class GetAllRequesterHandler : IRequestHandler<GetAllRequestersQuery, List<Requester>>
    {
        private readonly IRequesterRepository _requesterRepository;

        public GetAllRequesterHandler(IRequesterRepository requesterRepository)
        {
            _requesterRepository = requesterRepository;
        }

        public async Task<List<Requester>> Handle(GetAllRequestersQuery request, CancellationToken cancellationToken)
        {
            var requesters = await _requesterRepository.GetAll().ToListAsync();
            return requesters;
        }
    }
}
