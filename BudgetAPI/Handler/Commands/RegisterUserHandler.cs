﻿using Common.Services;
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class RegisterUserHandler : IRequestHandler<RegisterUserCommand, Result<RegisterDTO>>
    {
        private IUserRepository _userRepository;
        private IJWTService _jWTService;
        private IConfiguration _config;

        public RegisterUserHandler(IUserRepository userRepository,
            IJWTService jWTService,
            IConfiguration config)
        {
            _userRepository = userRepository;
            _jWTService = jWTService;
            _config = config;
        }

        public async Task<Result<RegisterDTO>> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var (user, errors) = await _userRepository.AddAsync(request.EmailAddress, request.Password);

            if (user is null) return new Result<RegisterDTO> { Errors = errors, Success = false };

            var token = _jWTService.GenerateJSONWebToken(_config["JwtSettings:Secret"], _config["JwtSettings:Issuer"]);

            return new Result<RegisterDTO>
            {
                ResultData = new RegisterDTO { Token = token },
                Success = true
            };
        }
    }
}