﻿using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class CreateRequesterHandler : IRequestHandler<CreateRequesterCommand, RequesterDTO>
    {
        private readonly IRequesterRepository _requestRepository;

        public CreateRequesterHandler(IRequesterRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }
        public async Task<RequesterDTO> Handle(CreateRequesterCommand command, CancellationToken cancellationToken)
        {
            var requester = await _requestRepository.CreateAsync(command.EmailAddress, command.MiddleName, command.FirstName, command.LastName, command.MobileNumber, command.TitleUniqueId, command.ExtensionName);

            if (requester is null) return null;

            return new RequesterDTO
            {
                EmailAddress = requester.EmailAddress,
                ExtensionName = requester.ExtensionName,
                FirstName = requester.FirstName,
                LastName = requester.LastName,
                MiddleName = requester.MiddleName,
                MobileNumber = requester.MobileNumber,
                RequesterUniqueId = requester.UniqueId,
                TitleUniqueId = requester.TitleUniqueId,
            };
        }
    }
}
