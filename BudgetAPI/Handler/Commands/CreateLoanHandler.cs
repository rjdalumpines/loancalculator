﻿
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class CreateLoanHandler : IRequestHandler<CreateLoanCommand, LoanRequesterDTO>
    {
        private readonly ILoanRepository _loanRepository;
        private readonly IRequesterRepository _requesterRepository;

        public CreateLoanHandler(IRequesterRepository requesterRepository, ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
            _requesterRepository = requesterRepository;
        }

        public async Task<LoanRequesterDTO> Handle(CreateLoanCommand command, CancellationToken cancellationToken)
        {
            var loan = await _loanRepository.CreateAsync(command.InterestRate, command.MonthlyAmortization, command.Principal, command.RequesterUniqueId, command.Terms);

            var requester = await _requesterRepository.GetById(command.RequesterUniqueId);

            if (loan is null || requester is null) return null;

            return new LoanRequesterDTO
            {
                EmailAddress = requester.EmailAddress,
                FirstName = requester.FirstName,
                ExtensionName = requester.ExtensionName,
                LastName = requester.LastName,
                MiddleName = requester.MiddleName,
                MobileNumber = requester.MobileNumber,
                MonthlyAmortization = loan.MonthlyAmortization,
                Principal = loan.Principal,
                Terms = loan.Terms,
                TitleUniqueId = requester.TitleUniqueId,
                RequesterUniqueId = requester.UniqueId,
                InterestRate = loan.InterestRate,
                LoanUniqueid = loan.UniqueId
            };
        }
    }
}
