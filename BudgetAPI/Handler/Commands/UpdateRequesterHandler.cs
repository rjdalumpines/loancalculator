﻿using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using LoanCalculatorDomain.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class UpdateRequesterHandler : IRequestHandler<UpdateRequesterCommand, Result<RequesterDTO>>
    {
        private readonly IRequesterRepository _requestRepository;

        public UpdateRequesterHandler(IRequesterRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        public async Task<Result<RequesterDTO>> Handle(UpdateRequesterCommand request, CancellationToken cancellationToken)
        {
            var requester = await _requestRepository.UpdateAsync(request.EmailAddress,
                request.MiddleName,
                request.FirstName,
                request.LastName,
                request.MobileNumber,
                request.TitleUniqueId,
                request.ExtensionName,
                request.RequesterUniqueId);

            if (requester is null) return new Result<RequesterDTO> { Errors = new List<string> { "" }, Success = false };

            return new Result<RequesterDTO>
            {
                ResultData = new RequesterDTO
                {
                    EmailAddress = requester.EmailAddress,
                    ExtensionName = requester.ExtensionName,
                    FirstName = requester.FirstName,
                    LastName = requester.LastName,
                    MiddleName = requester.MiddleName,
                    MobileNumber = requester.MobileNumber,
                    RequesterUniqueId = requester.UniqueId,
                    TitleUniqueId = requester.TitleUniqueId
                },
                Success = true
            };
        }
    }
}
