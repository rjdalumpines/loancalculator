﻿using Common.Formula.Interfaces;
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class ComputeMonthlyAmortizationAndTotalInterestHandler : IRequestHandler<ComputeMonthlyAmortizationAndTotalInterestCommand, TotalInterestDTO>
    {
        private readonly IMonthlyMortgagePayments _monthlyMortgagePayments;

        public ComputeMonthlyAmortizationAndTotalInterestHandler(IMonthlyMortgagePayments monthlyMortgagePayments)
        {
            _monthlyMortgagePayments = monthlyMortgagePayments;
        }

        public async Task<TotalInterestDTO> Handle(ComputeMonthlyAmortizationAndTotalInterestCommand command, CancellationToken cancellationToken)
        {
            var (MonthlyMortgage, TotalInterest) = await Task.FromResult(_monthlyMortgagePayments.GetMonthlyMortgageAndTotalInterest(command.InterestRate, command.Principal, command.Terms));

            return new TotalInterestDTO
            {
                MonthlyMortgage = MonthlyMortgage,
                Terms = command.Terms,
                Principal = command.Principal,
                TotalInterest = TotalInterest
            };
        }
    }
}
