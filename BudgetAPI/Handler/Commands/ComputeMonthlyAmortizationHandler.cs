﻿using Common.Formula.Interfaces;
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler
{
    public class ComputeMonthlyAmortizationHandler : IRequestHandler<ComputeMonthlyAmortizationCommand, AmortizationDTO>
    {
        private readonly IMonthlyMortgagePayments _monthlyMortgagePayments;

        public ComputeMonthlyAmortizationHandler(IMonthlyMortgagePayments monthlyMortgagePayments)
        {
            _monthlyMortgagePayments = monthlyMortgagePayments;
        }
        public async Task<AmortizationDTO> Handle(ComputeMonthlyAmortizationCommand command, CancellationToken cancellationToken)
        {
            var monthlyMortgage = await Task.FromResult(_monthlyMortgagePayments.GetMonthlyMortgage(command.InterestRate, command.Principal, command.Terms));

            return new AmortizationDTO
            {
                MonthlyAmortization = monthlyMortgage,
                InterestRate = command.InterestRate,
                Principal = command.Principal,
                Terms = command.Terms,
            };
        }
    }
}
