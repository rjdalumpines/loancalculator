﻿using Common.Formula.Interfaces;
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class GetAmortizationScheduleHandler : IRequestHandler<CreateAmortizationScheduleCommand, List<AmortizationScheduleDTO>>
    {
        private readonly IMonthlyAmortizationSchedule _monthlyAmortizationSchedule;

        public GetAmortizationScheduleHandler(IMonthlyAmortizationSchedule monthlyAmortizationSchedule)
        {
            _monthlyAmortizationSchedule = monthlyAmortizationSchedule;
        }

        public async Task<List<AmortizationScheduleDTO>> Handle(CreateAmortizationScheduleCommand command, CancellationToken cancellationToken)
        {
            var schedule = await Task.FromResult(_monthlyAmortizationSchedule.GetMonthlyAmortizationSchedule(command.Interest, command.Principal, command.MonthlyAmortization, command.Terms)
                                                 .Select(a => new AmortizationScheduleDTO
                                                 {
                                                     InterestPaid = a.InterestPaid,
                                                     MonthlyAmortization = command.MonthlyAmortization,
                                                     Principal = a.Principal,
                                                     RunningBalance = a.RunningBalance,
                                                     RunningInterest = a.RunningInterest
                                                 }));
            return schedule.ToList();
        }
    }
}
