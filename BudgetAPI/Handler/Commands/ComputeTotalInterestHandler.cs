﻿using Common.Formula.Interfaces;
using LoanCalculatorAPI.Commands;
using LoanCalculatorAPI.Model;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class ComputeTotalInterestHandler : IRequestHandler<ComputeTotalInterestCommand, TotalInterestDTO>
    {
        private readonly IMonthlyMortgagePayments _monthlyMortgagePayments;

        public ComputeTotalInterestHandler(IMonthlyMortgagePayments monthlyMortgagePayments)
        {
            _monthlyMortgagePayments = monthlyMortgagePayments;
        }

        public async Task<TotalInterestDTO> Handle(ComputeTotalInterestCommand command, CancellationToken cancellationToken)
        {
            var totalInterest = await Task.FromResult(_monthlyMortgagePayments.GetTotalInterest(command.Terms, command.MonthlyAmortization, command.Principal));

            return new TotalInterestDTO
            {
                TotalInterest = totalInterest,
                MonthlyMortgage = command.MonthlyAmortization,
                Principal = command.Principal,
                Terms = command.Terms
            };
        }
    }
}
