﻿using LoanCalculatorAPI.Queries;
using LoanCalculatorAPI.Model;
using LoanCalculatorRepository.Repository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Handler.Commands
{
    public class GetUserHandler : IRequestHandler<GetUserQuery, Result<RequesterDTO>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IRequesterRepository _requesterRepository;

        public GetUserHandler(IUserRepository userRepository, IRequesterRepository requesterRepository)
        {
            _userRepository = userRepository;
            _requesterRepository = requesterRepository;
        }

        public async Task<Result<RequesterDTO>> Handle(GetUserQuery command, CancellationToken cancellationToken)
        {
            var (user, errors) = await _userRepository.GetByEmailAndPasswordAsync(command.EmailAddress, command.Password);

            if (user is null) return new Result<RequesterDTO> { Success = false, Errors = errors};

            var requester = await _requesterRepository.GetById(user.UniqueId);

            if (requester is null) return new Result<RequesterDTO> { Success = false, Errors = errors };

            return new Result<RequesterDTO>
            {
                ResultData = new RequesterDTO
                {
                    EmailAddress = requester.EmailAddress,
                    ExtensionName = requester.ExtensionName,
                    FirstName = requester.FirstName,
                    LastName = requester.LastName,
                    MiddleName = requester.MiddleName,
                    MobileNumber = requester.MobileNumber,
                    RequesterUniqueId = requester.UniqueId,
                    TitleUniqueId = requester.TitleUniqueId
                },
                Success = true
            };
        }
    }
}
