﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Model
{
    public class TotalInterestDTO
    {
        public int Terms { get; set; }

        public decimal MonthlyMortgage { get; set; }

        public decimal Principal { get; set; }

        public decimal TotalInterest { get; set; }
    }
}
