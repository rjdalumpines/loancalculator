﻿using System.Collections.Generic;

namespace LoanCalculatorAPI.Model
{
    public class Result<T> where T : class
    {
        public List<string> Errors { get; set; }
        public bool Success { get; set; }
        public T ResultData { get; set; }
    }
} 