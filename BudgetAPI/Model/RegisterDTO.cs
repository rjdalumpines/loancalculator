﻿using System.Collections.Generic;

namespace LoanCalculatorAPI.Model
{
    public class RegisterDTO
    {
        public string Token { get; set; }
    }
}