﻿using System;

namespace LoanCalculatorAPI.Model
{
    public class LoanRequesterDTO : RequesterDTO
    {

        public int Terms { get; set; }

        public decimal InterestRate { get; set; }

        public decimal Principal { get; set; }

        public decimal MonthlyAmortization { get; set; }

        public Guid LoanUniqueid { get; set; }
    }
}
