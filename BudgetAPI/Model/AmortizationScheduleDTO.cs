﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Model
{
    public class AmortizationScheduleDTO
    {
        public decimal InterestPaid { get; set; }

        public decimal Principal { get; set; }

        public decimal MonthlyAmortization { get; set; }

        public decimal RunningInterest { get; set; }

        public decimal RunningBalance { get; set; }

    }
}
