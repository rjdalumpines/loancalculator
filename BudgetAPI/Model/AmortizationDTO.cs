﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Model
{
    public class AmortizationDTO
    {
        public int Terms { get; set; }

        public decimal InterestRate { get; set; }

        public decimal Principal { get; set; }

        public decimal MonthlyAmortization { get; set; }
    }
}
