﻿using LoanCalculatorAPI.Model;
using System.Threading.Tasks;

namespace LoanCalculatorAPI.Services.IdentityService
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterAsync(string email, string username, string password);
    }
}