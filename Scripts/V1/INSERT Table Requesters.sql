DECLARE @UNIQUEID UNIQUEIDENTIFIER;
SET @UNIQUEID = NEWID();

INSERT INTO [dbo].[Requester] (UniqueId,CreatedBy, FirstName,LastName,MiddleName, ExtensionName,EmailAddress,IsDeleted,DateCreated,TitleUniqueId,MobileNumber)
VALUES (@UNIQUEID, @UNIQUEID,'Tim','Corey','Marvins', 'Jr','timcorey@email.com',0,GETDATE(),'102D4FDE-8EC4-47DE-B8D3-5BBD7C48BB9E','639560000001')

INSERT INTO [dbo].[Requester] (UniqueId,CreatedBy, FirstName,LastName,MiddleName, ExtensionName,EmailAddress,IsDeleted,DateCreated,TitleUniqueId,MobileNumber)
VALUES (NEWID(), @UNIQUEID,'James','Ingram','Pelt', 'Sr','jamespingram@email.com',0,GETDATE(),'102D4FDE-8EC4-47DE-B8D3-5BBD7C48BB9E','639560000002')