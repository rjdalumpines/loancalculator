CREATE TABLE [dbo].[User]
(
	Id INT IDENTITY (1,1) NOT NULL,
	v UNIQUEIDENTIFIER  NOT NULL,	
	EmailAddress NVARCHAR(MAX) NOT NULL,		
	PasswordHash NVARCHAR(MAX) NOT NULL,	
	IsDeleted BIT NOT NULL,
	DateCreated DATETIME NOT NULL,
	CreatedBy UNIQUEIDENTIFIER  NOT NULL


 CONSTRAINT PK_User PRIMARY KEY CLUSTERED 
(
  [UniqueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
