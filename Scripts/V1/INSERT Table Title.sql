DECLARE @USERUNIQUEID UNIQUEIDENTIFIER;
SET @USERUNIQUEID = NEWID();

INSERT INTO [dbo].[Title] (UniqueId,CreatedBy,IsDeleted,DateCreated, Honorific, Description)
VALUES (NEWID(), @USERUNIQUEID,0,GETDATE(),'Mr.','for men, regardless of marital status, who do not have another professional or academic title, an abbreviation of Mister.')

INSERT INTO [dbo].[Title] (UniqueId,CreatedBy,IsDeleted,DateCreated, Honorific, Description)
VALUES (NEWID(), @USERUNIQUEID,0,GETDATE(),'Ms.','for girls, unmarried women and (in the UK) married women who continue to use their maiden name, an abbreviation of Miss.')

INSERT INTO [dbo].[Title] (UniqueId,CreatedBy,IsDeleted,DateCreated, Honorific, Description)
VALUES (NEWID(), @USERUNIQUEID,0,GETDATE(),'Mrs.','for married women who do not have another professional or academic title, an abbreviation of Mistress.')