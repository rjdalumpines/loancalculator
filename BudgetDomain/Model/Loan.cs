﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LoanCalculatorDomain.Model
{
    public class Loan : BaseModel
    {
        [Required]
        public int Terms { get; set; }

        [Required]
        public decimal InterestRate { get; set; }

        [Required]
        public decimal Principal { get; set; }

        [Required]
        public decimal MonthlyAmortization { get; set; }

        public Guid RequesterUniqueId { get; set; }
    }
}
