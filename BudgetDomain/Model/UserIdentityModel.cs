﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using LoanCalculatorDomain.Model;

namespace LoanCalculatorDomain.Model
{
    public class UserIdentityModel : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string PasswordHash { get; set; }

        [Required]
        [MaxLength(200)]
        public string EmailAddress { get; set; }
    }
}
