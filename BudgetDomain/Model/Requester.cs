﻿using LoanCalculatorDomain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LoanCalculatorDomain.Model
{
    [Table("Requester")]
    public class Requester : BaseModel
    {
        [Required]
        [MaxLength(200)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(200)]
        public string LastName { get; set; }

        [MaxLength(200)]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(20)]
        public string ExtensionName { get; set; }

        [Required]
        [MaxLength(20)]
        public string MobileNumber { get; set; }

        [Required]
        [MaxLength(200)]
        public string EmailAddress{ get; set; }

        public Guid? TitleUniqueId { get; set; }

    }
}
