﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LoanCalculatorDomain.Model
{
    public class BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public Guid UniqueId { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        [Required]
        public Guid CreatedBy { get; set; }
    }
}
