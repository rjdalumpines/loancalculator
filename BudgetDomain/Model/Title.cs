﻿using LoanCalculatorDomain.Model;
using System.ComponentModel.DataAnnotations;

namespace LoanCalculatorDomain.Model
{
    public class Title: BaseModel
    {
        [Required]
        [MaxLength(50)]
        public string Honorific { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
