﻿using Common.Formula;
using System;
using Xunit;

namespace LoanCalculatorTests.CalculateMonthlyMortgagePaymens
{
    public class CalculateMonthlyMortgagePaymentsTest
    {
        public readonly MortgagePayments MortgagePayments;

        public CalculateMonthlyMortgagePaymentsTest()
        {
            MortgagePayments = new MortgagePayments();
        }

        [Theory]
        [InlineData(4.5, 165_000, 30)]
        public void TestMonthyMortageScenario_1(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var expectedValue = 836.03M;

            var mortage = MortgagePayments.GetMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPayments);
            var result = decimal.Round(mortage, 2, MidpointRounding.AwayFromZero);

            Assert.Equal(expectedValue, result);
        }

        [Theory]
        [InlineData(4.5, 165_000, 15)]
        public void TestMonthyMortageScenario_2(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var expectedValue = 1_262.24M;

            var mortage = MortgagePayments.GetMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPayments);
            var result = decimal.Round(mortage, 2, MidpointRounding.AwayFromZero);

            Assert.Equal(expectedValue, result);
        }

        [Theory]
        [InlineData(18, 10_500, 1)]
        public void TestMonthyMortageScenario_3(decimal interestRate, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var expectedValue = 962.64M;

            var mortage = MortgagePayments.GetMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPayments);
            var result = decimal.Round(mortage, 2, MidpointRounding.AwayFromZero);

            Assert.Equal(expectedValue, result);


            //var expectedValue = 836.03M;

            //var mock = new Mock<IMonthlyMortgagePayments>();
            //mock.Setup(x => x.GetMonthlyMortgage(interestRate, principalLoanAmount, totalNumberOfPayments)).Returns(expectedValue);

            //Assert.NotEqual(expectedValue, result);
        }

    }
}
