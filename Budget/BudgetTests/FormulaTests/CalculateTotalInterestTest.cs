﻿using Common.Formula;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LoanCalculatorTests.FormulaTests
{
    public class CalculateTotalInterestTest
    {
        public readonly MortgagePayments MortgagePayments;

        public CalculateTotalInterestTest()
        {
            MortgagePayments = new MortgagePayments();
        }

        [Theory]
        [InlineData(1_262.2389265422141583335802350, 165_000, 15)]
        public void TestMonthyMortageScenario_1(decimal monthlyMortgage, decimal principalLoanAmount, int totalNumberOfPayments)
        {
            var expectedValue = 62_203.01M;

            var mortage = MortgagePayments.GetTotalInterest(totalNumberOfPayments, monthlyMortgage, principalLoanAmount);
            var result = decimal.Round(mortage, 2, MidpointRounding.AwayFromZero);

            Assert.Equal(expectedValue, result);
        }
    }
}
